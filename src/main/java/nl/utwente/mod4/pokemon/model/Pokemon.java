package nl.utwente.mod4.pokemon.model;

public class Pokemon extends NamedEntity {

    public float height;
    public float weight;
    public int hp;
    public int attack;
    public int spAttack;
    public int defense;
    public int spDefense;
    public int speed;

    public Pokemon() {
        super();
        height = 0;
        weight = 0;
        hp = 0;
        attack = 0;
        spAttack = 0;
        defense = 0;
        spDefense = 0;
        speed = 0;
    }
}
