package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import nl.utwente.mod4.pokemon.dao.PokemonDao;
import nl.utwente.mod4.pokemon.model.Pokemon;
import nl.utwente.mod4.pokemon.model.ResourceCollection;

@Path("/api/pokemon")

public class PokemonRoute {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<Pokemon> getPokemon(
            @QueryParam("sortBy") String sortBy,
            @QueryParam("pageSize") int pageSize,
            @QueryParam("pageNumber") int pageNumber)
    {
        int ps = pageSize > 0 ? pageSize : Integer.MAX_VALUE;
        int pn = pageNumber > 0 ? pageNumber : 1;

        Pokemon[] resources = PokemonDao.INSTANCE.getPokemon(ps, pn, sortBy).toArray(new Pokemon[0]);

        int total = PokemonDao.INSTANCE.getTotalPokemon();
        ps = resources == null ? 0 : resources.length;

        return new ResourceCollection<>(resources, ps, pn, total);
    }


    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Pokemon getPokemon(@PathParam("id") String id)
    {
        Pokemon pokemon = PokemonDao.INSTANCE.getPokemon(id);

        if (pokemon == null) {
            throw new NotFoundException("Pokemon '" + id + "' not found!");
        }

        return pokemon;
    }

    @DELETE
    @Path("/{id}")
    public void deletePokemon(@PathParam("id") String id)
    {
        PokemonDao.INSTANCE.delete(id);
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Pokemon updatePokemon(@PathParam("id") String id, Pokemon toReplace)
    {
        if (id == null || !id.equals(toReplace.id))
            throw new BadRequestException("Id mismatch.");

        return PokemonDao.INSTANCE.replace(toReplace);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Pokemon createPokemon(Pokemon pokemon)
    {
        if (pokemon.id != null || pokemon.created != null || pokemon.lastUpDate != null) {
            throw new BadRequestException("Invalid data. 'id', 'created', and 'lastUpDate' should not be provided by the client.");
        }

        return PokemonDao.INSTANCE.create(pokemon);
    }

}
