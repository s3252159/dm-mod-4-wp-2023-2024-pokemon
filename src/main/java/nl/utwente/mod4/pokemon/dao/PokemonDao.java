package nl.utwente.mod4.pokemon.dao;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.NotSupportedException;
import nl.utwente.mod4.pokemon.Utils;
import nl.utwente.mod4.pokemon.model.Pokemon;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public enum PokemonDao {

    INSTANCE;

    private static final String ORIGINAL_POKEMON = Utils.getAbsolutePathToResources() + "/original-pokemon-dataset.json";
    private static final String POKEMON = Utils.getAbsolutePathToResources() + "/pokemon.json";

    private HashMap<String, Pokemon> pokemon = new HashMap<>();

    public void delete(String id)
    {
        if (pokemon.containsKey(id)) {
            pokemon.remove(id);
        } else {
            throw new NotFoundException("Pokemon '" + id + "' not found.");
        }
    }

    public List<Pokemon> getPokemon(int pageSize,
                                    int pageNumber,
                                    String sortBy)
    {
        List<Pokemon> list = new ArrayList<>(pokemon.values());

        if (sortBy == null || sortBy.isEmpty() || "id".equals(sortBy)) {
            list.sort((pt1, pt2) -> Utils.compare(Integer.parseInt(pt1.id), Integer.parseInt(pt2.id)));
        } else if ("name".equals(sortBy)) {
            list.sort((pt1, pt2) -> Utils.compare(pt1.name, pt2.name));
        } else {
            throw new NotSupportedException("Only sorting by name is supported");
        }

        return (List<Pokemon>) Utils.pageSlice(list,pageSize,pageNumber);
    }

    public Pokemon getPokemon(String id)
    {
        var p = pokemon.get(id);

        if (p == null) {
            throw new NotFoundException("Pokemon '" + id + "' not found!");
        }

        return p;
    }

    public void load() throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        File source;

        if (existsPokemon()) {
            source = new File(POKEMON);
        } else {
            source = new File(ORIGINAL_POKEMON);
        }

        Pokemon[] arr = mapper.readValue(source, Pokemon[].class);

        for (Pokemon p : arr)
        {
            pokemon.put(p.id, p);
        }
    }

    private boolean existsPokemon()
    {
        File f = new File(POKEMON);
        return f.exists() && !f.isDirectory();
    }

    public void save() throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
        File destination = new File(POKEMON);

        mapper.writeValue(destination, pokemon.values());
    }

    public Pokemon create(Pokemon newPokemon)
    {
        String nextId = "" + (getMaxId() + 1);

        newPokemon.id = nextId;
        newPokemon.created = Instant.now().toString();
        newPokemon.lastUpDate = Instant.now().toString();
        pokemon.put(nextId, newPokemon);

        return newPokemon;
    }

    private int getMaxId()
    {
        Set<String> ids = pokemon.keySet();
        return ids.isEmpty() ? 0 : ids.stream()
                .map(Integer::parseInt)
                .max(Integer::compareTo)
                .get();
    }

    public Pokemon replace(Pokemon update)
    {
        if (!update.checkIsValid()) {
            throw new NotFoundException("Invalid pokemon.");
        }

        if (pokemon.get(update.id) == null) {
            throw new NotFoundException("Pokemon id '" + update.id + "' not found.");
        }

        update.lastUpDate = Instant.now().toString();
        pokemon.put(update.id, update);

        return update;
    }

    public int getTotalPokemon()
    {
        return pokemon.keySet().size();
    }

}
